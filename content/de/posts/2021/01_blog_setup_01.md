---
title: "Blog Setup Part 1"
date: 2021-10-04T08:13:49+02:00
draft: false
description: "Wie wurde dieser Blog aufgebaut"
featured: false
categories: [
  "IT",
]
series: [
  "Blog setup",
]
tags: [
    "blog",
    "hugo",
    "gitlab",
    "automation",
]
images: [
]
---

So, nachdem dies der erste Eintrag im neuen Blog ist. Möchte ich hier etwas über das Setup sprechen.

# Vorgeschichte

Es gab schon einmal einen Blog von 2012-2015, damals war es Wordpress mit allem tollen und nicht so tollen Features die es mit sich brachte.  
Aufgrund der Entwicklung von Wordpress zu diesem CMS Monster und dem damit verbundenen, immer größer werdenden, Wartungsaufwand entschied ich mich irgendwann die Segel zu streichen und den Blog einzustampfen.  
Deswegen wird es hier auch keinen alten Content geben, der ist mittlerweile sowieso komplett überholt.

# Was nimmt man denn in 2021 so?

Die erste große Frage war: Welches Tooling nimmt man denn heute so um seinen Blog mit möglichst wenig Aufwand zu fahren?  

Natürlich gibt es viele Möglichkeiten und alle sind auf ihre Art mächtig.  
Nur um hier ein paar zu nennen, die ich mir näher angeschaut habe:  

- [Jekyll](https://jekyllrb.com/)
- [Hugo](https://gohugo.io/)
- [Ghost](https://ghost.org/)
- [Pelican](https://blog.getpelican.com/)

Als Static Site Generators können die Tools mehr oder weniger alle das gleiche, es kann aber nur eines geben. Als müssen ein paar Kriterien her, nach denen ich aussortieren kann.  

- Verbreitung
- Aktivität
- Handhabbarkeit
- Wartungsaufwand

# Entscheidungsfindung

## Jekyll

Jekyll ist mir durch Freunde und Bekannte seit Jahren ein Begriff und scheint gut zu funktionieren.

- Verbreitung
  - sehr weit Verbreitet
- Aktivität
  - sehr aktive Entwicklung
- Handhabbarkeit
  - für Einsteiger einfach zu benutzen
  - In Ruby geschrieben => Probleme bei dauerhaftem Betrieb
- Wartungsaufwand
  - Gering, solange nichts Custom gemacht wird
  - Evtl Probleme durch Ruby

Das mag jetzt etwas merkwürdig klingen, mit Ruby bin ich nie warm geworden und habe daran auch kein wirkliches Interesse.  
Für mich war das ein sofortiges K.O., da ich keine Lust habe später Dinge zusammenzuhacken, wenn etwas nicht so funktioniert wie ich es brauche.

## Hugo

Auch Hugo ist mir immer mal wieder über den Weg gelaufen, vorallem durch [@dnsmichi](https://twitter.com/dnsmichi) und [everyonecancontribute.com](https://gitlab.com/everyonecancontribute/web/everyonecancontribute.gitlab.io)

- Verbreitung
  - sehr weit verbreitet
- Aktivität
  - sehr aktive Entwicklung
- Handhabbarkeit
  - für Einsteiger sehr einfach
  - einfaches aber mächtiges Templating System
  - sehr gute Tutorials
- Wartungsaufwand
  - sehr große Auswahl an Themes
    - eventuell Probleme wenn Theme nicht weiterentwickelt wird

## Ghost

Von Ghost, hatte ich irgendwann mal durch Zufall gehört, wirklich damit beschäftigt aber nie.

- Verbreitung
  - unbekannt
- Aktivität
  - vermutlich aktiv
- Handhabbarkeit
  - sehr mächtig und komplex
  - richtet sich eher an professionelle Contentcreator
- Wartungsaufwand
  - Hosting lässt sich günstig kaufen
  - Selfhosting ist durch Docker sehr einfach

## Pelican

Pelican wurde mir erst durch [@mutax](https://twitter.com/mutax) näher gebracht. Da es in Python geschrieben ist, war direkt etwas mehr Sympathie vorhanden.

- Verbreitung
  - unbekannt
- Aktivität
  - sehr aktive Entwicklung
- Handhabbarkeit
  - relativ einfach
  - sehr ausführliche Dokumentation
- Wartungsaufwand
  - große Fülle an Themes und Plugins
    - hohe Wahrscheinlichkeit, dass nicht mehr weiterentwickelt wird

## Die Entscheidung

Nach sehr langem hin und her und viel Ausprobieren, habe ich mich dann für Hugo entschieden.  
Einer der größten Faktoren dafür war, dass der Einstieg sehr einfach ist und trotz aktiver Entwicklung kaum "breaking Changes" passieren.  
Damit kann ich davon ausgehen, dass ich auf lange Zeit sehr wenig Wartungsaufwand haben werde.

Im nächsten Part werde ich dann darauf eingehen, wie ich Hugo nutze und warum Gitlab und Gitlab Pages etwas sehr geniales sind, wenn ich nicht meine eigene Infrastruktur betreiben möchte.