---
title: "Disable log4j JDNI Lookups"
date: 2021-12-10T16:51:11+01:00
draft: false
description: "Wie man JNDI Lookup in Jitsi deaktiviert"
featured: false
categories: [
  "IT"
]
tags: [
    "Java",
    "log4j",
    "CVE",
]
---

In diesem Blogpost wollte ich euch eigentlich anhand von Jitsi zeigen, wie man die Remote Code Execution in log4j deaktiviert.  

Es gibt hierfür mehrere Möglichkeiten dies zu tun.

## Startparameter

Wenn man ein aktuelles Javaprogramm am laufen hat, wie z.B. Jitsi, Graylog, Jenkins, haben diese ein sehr einfache Möglichkeit das Problem zu umgehen.  

Man muss nur folgenden Startparameter `-Dlog4j2.formatMsgNoLookups=true` unterbringen.  
Dies kann man entweder direkt im Service von Systemd machen, oder man schaut ob eine Environment Datei existiert, man findet sie je nach Betriebssystem entweder unter `/etc/default` oder `/etc/sysconfig`. Wenn man den Parameter dort an eine Zeile Anhängt, die `JAVA_OPTS` im Namen hat. Dann muss man einfach nur noch die Applikation neu starten und der Exploit sollte nicht mehr funktionieren.  
Je nach Art und Aufbau der Applikation, kann es auch sein, dass man es an einer anderen stelle eintragen muss, z.B. bei Applikationen die in einem Container laufen.  
Dort müssen sie entweder and Umgebungsvariable beim erstellen des Containers übergeben werden, oder in einer Konfigurationsdatei. Diese Informationen muss man sich aus der Dokumentation des Herstellers holen.  

**EDIT: Dieser Startparameter wurde erst mit der log4j Version 2.10.0 eingeführt. Sollte die Applikation eine ältere Variante benutzen, ist der Weg über den Startparameter nicht möglich.**

## Anpassen des Loggings

Die zweite Möglichkeit ist das Anpassen des Logformates, welches log4j verwendet. Dies sollte nur von jemandem vorgenommen werden, der versteht was dort geloggt wird. Da es sonst dazu kommen kann, das die Logs unlesbar werden.  

Wenn eine Javaapplikation log4j verwendet, dann liefert sie auch immer eine Konfiguration dafür mit. Diese befindet sich normalerweise in einer Datei mit dem Namen `log4j2.xml`.  
In dieser Datei gibt es typischerweise mindestens ein sogenanntes PatternLayout, dass den Aufbau der Lognachrichten beschreibt. 

```diff
1c1
<  <PatternLayout pattern="%d %-5p (%F:%L) - %m%n"/> 
---
>  <PatternLayout pattern="%d %-5p (%F:%L) - %m{nolookups}%n"/> 
```

Wenn man in diesem Patternlayout nun den Parameter `%m` durch `{nolookups}` ergänzt, wird verhindert, dass URL aufgelöst werden. Dadurch ist die Remote Code Execution unterbunden.  

## Schlusswort

Die beiden aufgezeigten Möglichkeiten sind natürlich nicht optimal. Die Lücke wurde in log4j ab der Version 2.14.1 geschlossen.  
Wenn man die Möglichkeit hat, sollte die verwendete Software so schnell wie möglich aktualisiert werden.