---
title: "Blog Setup Part 2"
date: 2021-10-18T17:07:02+02:00
draft: false
description: "Zweiter Part des Blog Setups"
featured: false
categories: [
  "IT",
]
series: [
  "Blog setup",
]
tags: [
    "DNS",
    "Gitlab",
    "Gitlab Pages",
    "Hugo",
    "automation",
]
images: [
]
---

Nachdem ich im letzten Part erklärt habe, wie ich zu der Entscheidung kam Hugo als Static Site Generator für den neuen Blog zu verwenden, geht es heute weiter mit dem  organisatorischen Aspekt.  

Folgende Fragen standen nach der Entscheidung für ein Tool noch im Raum:  

- Wo hoste ich den Blog?
- Wie wir der Blog erreichbar sein?
- Wie weit kann ich das automatisieren?

## Hosting

Hier hatte ich die Auswahl zwischen zwei Optionen, entweder ich hoste alles selbst, oder suche mir einen Hoster.  
Wenn ich das alles selbst hosten würde, dann müsste ich einen ganzen Rattenschwanz an Abhängigkeiten berücksichtigen, welcher dem Gedanken eines wartungsarmen Blogs entgegen steht.  
Gehen wir einmal anhand eines selbst gehosteten Gitlabs durch, was es bedeuten würde das alles selbst zu machen.  

### eigenes Hosting

Die erste Hürde beim eigenen Hosting entfällt in meinem Fall, da ich bereits über einen Root-Server bei Hetzner verfüge und somit keine Mehrkosten anfallen.
Jetzt brauche ich einen Ort um die Daten für den Blog abzulegen, vorzugsweise ein Gitlab, da hier die Automatisierung sehr angenehm und vertraut ist.  
Also muss ich ein Gitlab in der Community Edition auf einer eigenen VM aufsetzen.  
Dann benötige ich eine zweite VM welche den Gitlab-Runner hoste, diese sollte nach Möglichkeit soweit es geht abgeschottet sein, da der Runner Containerimages als Root ausführt.  
Sobald das alles erledigt ist, könnte ich nun die Gitlab interne Funktion der Pages benutzen, um meinen Blog zu hosten.  

Alles in allem, habe ich trotz eines hohen Automatisierungsgrades in der privaten Umgebung einen nicht zu unterschätzenden Aufwand von 1-2 Stunden pro Monat für die VMs und die Software, von Betriebssystemmigrationen, welche alle paar Jahre anstehen mal abgesehen.

### fremdes Hosting

Wie ich schon im [ersten Blogartikel](/posts/blog_setup_01/) erzählt habe, haben mich [@dnsmichi](https://twitter.com/dnsmichi) und [everyonecancontribute.com](https://gitlab.com/everyonecancontribute/web/everyonecancontribute.gitlab.io) sehr in meiner Entscheidung des Toolings beeinflusst.  
Also warum nicht von erfahrenen Menschen lernen und sich mal die offiziellen Gitlab Pages von [gitlab.com](gitlab.com) anschauen.  

### Entscheidung: Hosting

Es war sehr schnell klar, dass sich der Aufwand alles selbst zu hostens absolut nicht lohnt.  
Also war die erste Entscheidung schnell gefallen, das Blog wird auf den offiziellen Gitlab Pages gehostet.  

## Domain

Ich besitze seit einigen Jahren eine Domain, über welche ich meine privaten Tools erreichbar mache.  
Diese hat bewusst keine direkte Verknüpfung mit meinem Namen und wäre somit ungeeignet einen Blog zu hosten, auf dem ich über Themen die mich interessieren schreibe.
Es muss also eine neue Domain her, aber welche es gibt mittlerweile so viele?  
Zum Glück war die Entscheidung nicht allzu schwierig, da die meisten Domains mit meinem Nachnamen schon an einen Holzhausbauer in Schweden vergeben sind.  
Ich entschied mich also für `honka.dev`, aber wo bekomme ich die jetzt?  

### Hetzner

Als jahrelanger Kunde von Hetzner, schaut man natürlich zuerst dort.  
Und tatsächlich lässt sich die Domain dort buchen, allerdings schickt mich [https://www.hetzner.com/whois](https://www.hetzner.com/whois) auf ihr altes DNS-System, auf welchem ich keinen Account besitze.  
Also schnell einen Account geklickt und ein Interface, dass an den Anfang der 2000er erinnert zeigt sich mir.  
Es gibt dort auf den ersten Blick keine Verbindung zu [dns.hetzner.com](https://dns.hetzner.com), welches ich für meine anderen Domains benutze.  
Damit fällt Hetzner also komplett raus, ich möchte nicht mehrere Interfaces benutzen müssen, die auch noch ein sehr altbackenes Aussehen haben.  

### Cloudflare

Mir wurde empfohlen mal bei Cloudflare zu schauen, leider wird einem schnell mitgeteilt, dass es hier, zumindest für Deutschland, keinen Zugriff auf .dev Domains gibt.  
Also auch direkt raus.

### Google Domains

Jemand meinte ich sollte es mal bei Google versuchen.  
Ich bin kein großer Freund von Google, aber was soll man machen, wenn man schon bei zwei Anbietern nicht wirklich fündig geworden ist.  
Also auf [domains.google.com](https://domains.google.com) gegangen und geschaut ob die Domain frei ist.  
Da sie noch frei war, wies mich Google darauf hin, dass ihr Domain Service in Deutschland noch nicht für Privatanwender freigeschaltet ist.  
Nach einer einfachen Bestätigung, hat aber niemand mehr wirklich danach gefragt, ob ich die Domain für kommerzielle Zwecke nutzen möchte.  
Also einmal die Kreditkartendaten eingegeben und schon gehörte die Domain mir, keine Nachfragen, nur eine Bestätigungsmail des Registrars.

### Entscheidung: Domain

Die Domain liegt also bei Google, was definitiv nicht ideal ist und sicher irgendwie in ihre verwurstung von Daten zu Werbezwecken mit einfliest.  
Ich habe jetzt ein Jahr Zeit, eine brauchbare Alternative zu finden. Es gibt ja noch viele Registrare dort drausen.

## Automatisierung

Mit der vorherigen Entscheidung, den Blog auf den Gitlab Pages zu hosten, ist auch schnell klar, dass man so ziemlich alles automatisieren kann.  
Gitlab hat ein paar [Beispiele](https://gitlab.com/pages/hugo), wie eine Pipeline für Hugo aussehen kann und wenn man nicht viel braucht, dann funktioniert das auch alles sofort.  
Also warum nicht die Arbeit so weit es geht reduzieren und einfach das Beispiel etwas anpassen. Das war auch schnell erledigt und die erste Version des Blogs war live.