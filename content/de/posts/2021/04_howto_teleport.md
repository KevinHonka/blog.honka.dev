---
title: "Teleport, warum es die bessere Bastion ist."
date: 2021-11-08T15:23:19+01:00
draft: false
description: "Ein Artikel über das Aufsetzen von SSH Zugriff via Teleport"
featured: false
categories: [
  "IT"
]
series: [
  "Teleport"
]
tags: [
    "Teleport",
    "SSH",
    "ACCESS",
]
images: [
]
---

In diesem Blogpost werde ich erklären, _warum_ und _wie_ ich Teleport einsetze und wieso ihr es euch auch überlegen solltet.

Um das _'warum'_ zu erkären, sollte ich erst erklären was genau [Teleport][1] ist und wofür es gedacht ist.  
Teleport ist dafür gedacht, ähnlich einer [SSH Bastion][2] als zentrale, oder dezentrale Zugangsstelle zu Diensten in einem abgeschotteten Netzwerk zu dienen.  
Hierfür kann es für SSH, Webapplikationen, Kubernetes und Datenbanken(MongoDB, Postgresql) als Bastion dienen.  
Zu den Funktionen von Teleport gehört ein sehr ausgefeiltes Rollenkonzept, sowie die Möglichkeit eines sehr detaillierten Auditsystems, um alle Zugriffe nachvollziehen zu können.  

## Warum wird Teleport eingesetzt

Wie etwas weiter oben schon angerissen bietet Teleport deutlich modernere und einfachere Methoden als Bastion zu dienen und das nicht nur für SSH, sondern auch für andere Anwendungszwecke.  
Zum Zeitpunkt dieses Artikels verwende ich Teleport hauptsächlich um meine privaten VMs zu verwalten, sowie bei einem Kunden, der einen sehr hohen Schutzbedarf hat.  

Gerade das ausgefeilte Auditsystem, welches bei Bedarf über [BPF sogar die Systemcalls aufzeichnen kann][3], sowie das Teilen von SSH Sessions mit mehreren Leuten sind unheimlich hilfreich im Enterpriseumfeld um schnell reagieren zu können, wenn man etwas nachverfolgen oder debuggen muss.

## Teleport Setup

Das Einrichten von Teleport ist in seiner Standardkonfiguration sehr trivial, die [Setup Dokumentation][4] ist sehr gut verständlich und lässt sich gut nachverfolgen.  
Da ich bereits eine ältere SSH Bastion hatte, habe ich Teleport auf der gleichen VM installiert.  
Meine Konfiguration sieht wie folgend aus:  

```yaml

teleport:
  data_dir: /var/lib/teleport
  log:
    output: stderr
    severity: INFO
auth_service:
  enabled: "yes"
  authentication:
    type: local
    second_factor: off
  tokens:
  - "proxy,node,db,app:******************"
  cluster_name: "astosch.de"
  listen_addr: 0.0.0.0:3025
ssh_service:
  enabled: "yes"
  labels:
    env: hosted
  commands:
  - name: hostname
    command: [hostname]
    period: 1m0s
  - name: arch
    command: [uname, -p]
    period: 1h0m0s

proxy_service:
  enabled: "yes"
  listen_addr: 0.0.0.0:3023
  https_keypairs: []
  acme: {}
  public_addr: "<public_url>"

```

Wenn man die Dokumentation liest, sieht man hier direkt einige Unterschiede.  
Zum einen habe ich momentan noch die Zweifaktorauthentifizierung deaktiviert und ein statisches Token gesetzt, da es mit Ansible so etwas einfacher ist, die anderen Nodes aufzusetzen.  
Diese benötigen nämlich in ihrer Konfiguration das entsprechende Token, um sich dem Cluster anzuschliessen.  

```yaml

teleport:
  auth_token: **************
  log:
    output: stderr
    severity: INFO
    format:
      output: text
  ca_pin: ""
  auth_servers:
    - <authservice.public_addr>
auth_service:
  enabled: "no"
ssh_service:
  enabled: "yes"
  labels:
    env: hosted
  commands:
  - name: hostname
    command: [hostname]
    period: 1m0s
  - name: arch
    command: [uname, -p]
    period: 1h0m0s

```

Wenn man alles richtig gemacht hat, kann man sich nun auf der neuen Bastion einloggen und bekommt etwas, dass so ähnlich aussieht:

![Teleport Access Page](images/teleport/teleport_access_screen.jpg)  

Schon ist es _"fertig"_.  
Teleport kann als Bastion benutzt werden.  
Im nächsten Blogpost werde ich darauf eingehen, wie das Auditsystem von Teleport funktioniert und wie man Session mit mehreren Benutzern teilen kann.


[1]: https://goteleport.com/
[2]: https://goteleport.com/blog/ssh-bastion-host/
[3]: https://goteleport.com/docs/server-access/guides/bpf-session-recording/
[4]: https://goteleport.com/docs/getting-started/linux-server/