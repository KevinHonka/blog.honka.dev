---
title: "Blog Setup Part 3"
date: 2021-10-18T17:50:58+02:00
draft: false
description: "Dritter Part des Blog Setups"
featured: false
categories: [
  "IT",
]
series: [
  "Blog setup",
]
tags: [
    "DNS",
    "Gitlab",
    "Gitlab Pages",
    "Hugo",
    "automation",
]
images: [
]
---

So, jetzt fehlt nur noch der technische Teil, wie der Blog eigentlich funktioniert und das ist eigentlich ganz simpel.  

Als erstes habe ich mir ein Repository auf [gitlab.com](https://gitlab.com) angelegt.  
Wenn man das macht, gibt Gitlab einem direkt die Auswahl ein Templaterepository zu verwenden, wie z.B. ein Repository mit fertiger Pipeline für Hugo.  
Man kann also direkt loslegen, die Standardpipeline sieht wie folgend aus und passt ziemlich genau auf meinen Use-Case.

```yaml {linenos=false,linenostart=199}
image: registry.gitlab.com/pages/hugo:latest

variables:
  GIT_SUBMODULE_STRATEGY: recursive

test:
  script:
  - hugo
  rules:
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
```

Ich habe das dann noch etwas zusammengestrichen, da das Template etwas älter ist und neuere Funktionen von Gitlab nicht benutzt.

```yaml {linenos=false,linenostart=199}
image: registry.gitlab.com/pages/hugo:latest

variables:
  GIT_SUBMODULE_STRATEGY: recursive

test:
  script:
  - hugo
  except:
  - master

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - master
```

Wenn ich jetzt neuen Content in den Default Branch pushe, wird der Blog automatisch neu generiert. Das kostet mich überhaupt kein Aufwand und ich kann mich voll aus schreiben konzentrieren.  

Als nächstes müssen wir noch eine Verbindung von der neuen Domain zu Gitlab herstellen, auch das ist sehr einfach.
Wenn man auf der linken Seite unter Settings auf Pages navigiert wird einem folgendes angezeigt:

![](/images/blog_setup/gitlab_pages_setup.png)

Dort klickt man auf New Domain und kann seine Domain, die man für den Blog verwenden möchte eingeben.

![](/images/blog_setup/gitlab_pages_setup_2.PNG)

Wenn man jetzt noch das automatische Zertifikatsmanagement aktiviert, hat man sogar ein kostenloses Zertifikat von Let's Encrypt um das man sich nicht kümmern muss, da Gitlab automatisch für die Verlängerung sorgt.

Ist alles richtig eingetragen, bekommt man eine übersicht, was jetzt noch zu tun ist.

![](/images/blog_setup/gitlab_pages_setup_3.PNG)

Es muss beim DNS Provider ein CNAME Record angelegt werden, der auf Gitlab Pages zeigt und ein verification code muss noch im DNS hinterlegt werden, damit Gitlab sicherstellen kann, dass man auch wirklich die Domain besitzt und nicht irgendwelche schlimmen Dinge anstellen möchte.  
Wenn man alle Einstellungen vorgenommen hat, kann man auf den Verified/Not Verified Knopf drücken um alles zu überprüfen.  
Ist alles ok, drückt man auf Save Changes und alles ist fertig.  
Es kann je nach aufkommen von Anfragen etwas länger dauern, bis das Zertifikat geholt wird. Wenn nicht gerade wieder so ein Problem wie mit dem invaliden Root-CA von Let's Encrypt besteht, sollte spätestens nach einer Stunde ein Zertifikat eingebunden sein.  

Damit ist das Setup für den Blog auch fertig. Man kann jetzt loslegen und ohne einen Gedanken an das Setup zu verschwenden, nach Lust und Laune schreiben.  

Ich hoffe dieser kleine Einblick zeigt, dass es nicht immer das mächtige große Setup mit Wordpress oder ähnlichem sein muss, wenn man nur ein kleines Blog betreiben möchte.